# odoo-portainer

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## step 1 Run portainer in docker

command:

```
docker compose up
```

## step 2 Add config-odoo.yml to stacks

1. In the Portainer dashboard, click on "Stacks" in the left sidebar menu.
2. Click on the "Add stack" button at the top-right corner of the page.
3. Provide a name for your Stack (e.g., "odoo").
4. In the "Web editor" tab, copy and paste the contents of the config-odoo.yml file below.
5. Click on the "Deploy the stack" button at the bottom of the page. Portainer will now create the necessary 
6. containers and networks, and start the services defined in the config-odoo.yml file.

```
version: '3.8'

services:

  postgres:
    container_name: odoo-postgres
    image: postgres:14.1-alpine
    networks:
      - web
    ports:
      - 5432:5432
    environment:
      POSTGRES_DB: postgres
      POSTGRES_USER: odoo
      POSTGRES_PASSWORD: secret
    volumes:
      - postgres-data:/var/lib/postgresql/data
    restart: always

  odoo:
    container_name: odoo
    image: odoo:16.0
    networks:
      - web
    environment:
      HOST: postgres
      USER: odoo
      PASSWORD: secret
    depends_on:
      - postgres
    ports:
      - 8069:8069
    volumes:
      - config:/etc/odoo
      - extra-addons:/mnt/extra-addons
      - data:/var/lib/odoo
    restart: always
    links:
      - postgres

volumes:
  postgres-data:
  config:
  extra-addons:
  data:

networks:
  web:
    external: true
    name: web
```

